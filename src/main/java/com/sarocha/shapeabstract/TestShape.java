/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.shapeabstract;

/**
 *
 * @author Sarocha
 */
public class TestShape {

    public static void main(String[] args) {
        Circle cir1 = new Circle(4.5);
        Circle cir2 = new Circle(5.2);
        Circle cir3 = new Circle(1.5);
        Rectangle rec1 = new Rectangle(3, 2);
        Rectangle rec2 = new Rectangle(4, 3);
        Square sq1 = new Square(4);
        Square sq2 = new Square(2);
        System.out.println(cir1);
        System.out.println(cir2);
        System.out.println(cir3);
        System.out.println(rec1);
        System.out.println(rec2);
        System.out.println(sq1);
        System.out.println(sq2);

        Shape[] shapes = {cir1, cir2, cir3, rec1, rec2, sq1, sq2};
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].getName() + " area : " + shapes[i].calArea());
        }

    }

}
